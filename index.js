
// 1
// not posted to page.

let arr = [1,2,3,4,5,[6,7,8,9,10]];
let flat = arr.flat();
console.log(flat);
console.log(arr.map(item=> item));

// 2 
// not posted to page.

let arr2 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
// add new items to the end of the array
// method # 1
arr2.push(11);
console.log(arr2);

// method # 2
// using rest operator to add new items to the end of the array
arr2 = [...arr2, 12];
console.log(arr2);

// method # 3
// using rest operator to add new items to the end of the array
arr2[arr2.length] = 13;

console.log(arr2);


// 3
// not posted to page.
if({a:1} == {a:1}){
    console.log('true');
}else{
    console.log('false');
}
// Result: false

if([1] == [1]){
    console.log('true');
}else{
    console.log('false');
}
// Result: false


// check the execution time of the code
console.time('time');
for(let i = 0; i < 1e6; i++){
    // do nothing
}
console.timeEnd('time');

// #TheWorldOfJavascript


function sayHi() {
    console.log(name);
    console.log(age);
    var name = 'Lydia';
    let age = 21;
}

sayHi();

// Result: 
// an error will be thrown
// ReferenceError: Cannot access 'age' before initialization


// #TheWorldOfJavascript