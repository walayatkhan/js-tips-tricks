// if (3 > 2 > 1) console.log(true);
// else console.log(false);

// Result: false
// because
// 3 > 2 it will return true
// true > 1 it will return false
// so the result will be false

// #2
// not yet uploaded

// let arr = [1, 5, 6, 78, 5, 4, 4, 81, 5, 56, 66, 66, 88];
// console.log(Math.max(...arr));
// console.log(Math.min(...arr));
// max: 88
// min: 1

// #3
// not yet uploaded

// let exampleArray = [
//   "hi",
//   "hello",
//   null,
//   "welcome",
//   null,
//   "to",
//   null,
//   "the",
//   null,
//   "world",
// ];
// exampleArray = exampleArray.filter((n) => n);

// console.log(exampleArray);
// Result: [ "hi", "hello", "welcome", "to", "the", "world" ]

// #TheWorldOfJavascript

// #4
// not yet uploaded

// var array = [1, 2, 3, 4, 5, 6];
// console.log(array.length); // 6
// array.length = 3;
// console.log(array.length); // 3
// console.log(array); // [1,2,3]
// array.length = 0; // reset the length to 0
// console.log(array); // []

// #TheWorldOfJavascript

// const arrayItems = ["a", "b", ["c", "d", ["e", "f"]]];
// const flatArray = arrayItems.flat(2); // Flatten until depth 2

// flatArray: ['a', 'b', 'c', 'd', 'e', 'f'];
// #TheWorldOfJavascript

// const stringNumber = "15";
// // stringNumber: '15'
// const amount = +stringNumber;
// console.log(amount); // 15

// const numberAmount = (num) =>
//   num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
// const money = numberAmount(1000000);
// console.log(money); // 1,000,000

// #TheWorldOfJavascript

// const StartScore = (rate) => "★★★★★☆☆☆☆☆".slice(5 - rate, 10 - rate);
// const start = StartScore(4);
// console.log(start);
// rating => '★★★★☆'

// #TheWorldOfJavascript

// const executeFunc = (e) => console.log("array items", e);
// const arrayItems = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

// check if arrayItems is not empty then execute the executeFunc function.
// arrayItems.length && executeFunc(arrayItems);

// #TheWorldOfJavascript

// const numbers = [2, 3, 4, 15, 7, 20, 8];
// const sum = numbers.reduce((a, b) => a + b);
// console.log(sum); // 59

// #TheWorldOfJavascript

// swap the values of two variables without using a third variable.
// const a = 1;
// const b = 2;
// [a, b] = [b, a];
// console.log(a); // 2
// console.log(b); // 1

// #TheWorldOfJavascript

// const arrayDestructure = [0, 1, [2, 3, [4, 5]]];
// const [a, b, [c, d, [e, f]]] = arrayDestructure;
// console.log(a, b, c, d, e, f); // 0 1 2 3 4 5

// // Example 1
// const testFunction = function (...args) {
//   return args.join("");
// };
// const result = testFunction(2021, 2022);
// console.log(result); // ?

// // Example 2
// const testFunction1 = function () {
//   return arguments[0] + arguments[1];
// };
// const result1 = testFunction1(2021, 2022);
// console.log(result1); // ?

// // Example 3
// const testFunction3 = function () {
//   return arguments.join("");
// };
// const result3 = testFunction3(2021, 2022);
// console.log(result3); // ?

// const constructor = function () {};
// const result = constructor.prototype.constructor === constructor;
// console.log(result);

// // guess the result: true || false ?

// const plus = +0;
// const minus = -0;
// console.log(plus === minus); // true
// console.log(Object.is(plus, minus)); // false

// const obj = {};
// const result = "toString" in obj;

// console.log(result); //true or false ?

// #TheWorldOfJavascript

// let number = 5;
// const promise = new Promise((resolve) => {
//   number = 8;
//   resolve(100);
// });

// const result = number;
// console.log(result); // 8

// // #TheWorldOfJavascript

// let arr = ["5", "2", "9"];
// console.log(arr.reduce((a, b) => a + b)); // 529 - return wrong result.
// console.log(arr.reduce((a, b) => parseInt(a) + parseInt(b))); // 16
// console.log(arr.reduce((a, b) => Number(a) + Number(b))); // 16

// // shorten and fastest way to do the same thing.
// console.log(arr.reduce((a, b) => a * 1 + b * 1)); // 16
// console.log(arr.reduce((a, b) => +a + +b)); // 16
// console.log(arr.reduce((a, b) => ~~a + ~~b)); // 16

// // #TheWorldOfJavascript
// // #SpreadTheKnowledge
// // #Javascript

// let x = 2;
// let y = 2;
// let result = (x <<= y);   // left shift
// let result2 = (x >>= y); // right shift
// let result3 = (x &= 10);  // and

// console.log(result);  // 8
// console.log(result2); // 2
// console.log(result3); // 2

// Promises
// const promise1 = Promise.resolve("resolved");
// const promise2 = Promise.reject("rejected");
// const promise3 = Promise.resolve("resolved");

// const check = Promise.all([promise1, promise2, promise3])
//   .then((values) => {
//     console.log("resolved.....", values);
//   })
//   .catch((err) => {
//     console.log("rejected...", err);
//   });

// TheWorldOfJavascript
// #spreadTheKnowledge

// Promises
// const promise1 = Promise.resolve("resolved first");
// const promise2 = Promise.resolve("resolved second");
// const promise3 = Promise.resolve("resolved third");

// const check = Promise.all([promise1, promise2, promise3])
//   .then((values) => {
//     console.log("resolved.....", values);
//   })
//   .catch((err) => {
//     console.log("rejected...", err);
//   });

// TheWorldOfJavascript
// #spreadTheKnowledge

// const promise1 = Promise.resolve("resolved first");
// const promise2 = Promise.reject("rejected second");
// const promise3 = Promise.resolve("resolved third");

// const check = Promise.allSettled([promise1, promise2, promise3])
//   .then((values) => {
//     console.log("values here.....", values);
//   })
//   .catch((err) => {
//     console.log("error here...", err);
//   });

//   // Result:
//   [
//     { status: 'fulfilled', value: 'resolved first' },
//     { status: 'rejected', reason: 'rejected second' },
//     { status: 'fulfilled', value: 'resolved third' }
//   ]

// TheWorldOfJavascript
// #spreadTheKnowledge

// find the higest frequent alphbitic number in the string.
// const text = "abcddefda1111111333333333";
// let onlyAlp = text.match(/[a-z]/g);

// let higestAlp = "";
// let maxNumber = 0;

// onlyAlp.forEach((char) => {
//   //   console.log(onlyAlp.toString("").split(char));
//   let charLength = onlyAlp.toString("").split(char).length;

//   if (charLength > maxNumber) {
//     maxNumber = charLength;
//     higestAlp = char;
//   }
// });

// console.log(higestAlp); // d

// TheWorldOfJavascript
// #spreadTheKnowledge

// find most frequent element in the array.
// function mostFrequent(arr, n) {
//   // Sort the array
//   arr.sort();

//   // find the max frequency using linear
//   // traversal
//   let max_count = 1,
//     res = arr[0];
//   let curr_count = 1;

//   for (let i = 1; i < n; i++) {
//     if (arr[i] == arr[i - 1]) curr_count++;
//     else curr_count = 1;

//     if (curr_count > max_count) {
//       max_count = curr_count;
//       res = arr[i - 1];
//     }
//   }

//   return res;
// }
// let arr = [1, 2, 3, 4, 5, 5, 5, 5, 6, "A", "b", "c", "c", "c", "c", "c"];
// console.log(mostFrequent(arr, arr.length)); // c

// TheWorldOfJavascript
// #spreadTheKnowledge

// create a big int number.
// let n = 25;
// let result = BigInt(1);

// for (let i = n; i >= 1; i--) {
//   result *= BigInt(i);
// }
// console.log(result.toString()); // 15511210043330985984000000

// loog best practice.
let arr = [1, 2, 3, 4, 5, 5, 5, 5, 6, "A", "b", "c", "c", "c", "c", "c"];

for (let i = 0; i < arr.length; i++) {} // bad practice.
// The bad code accesses the length property of an array each time the loop is iterated.

let length = arr.length; // best practice.
for (let i = 0; i < length; i++) {}

// TheWorldOfJavascript
// #spreadTheKnowledge
