const str =
  "Lorem Ipsum is simply dummy text of the printing and typesetting industry. tax";
const number = "1234567890";
const number2 = "1414141414141414";
let text = "Give 100%!";

// find dummy in the string.

console.log(str.match(/dummy/g)); // ['dummy]

console.log(/dummy/g.test(str)); // true
console.log(/dummy/g.exec(str)); // true
console.log(/dummy/g.toString()); // /dummy/g, return the string value of regular expression.

console.log(/[1-5]/.test(number)); // true
console.log(/[1-5]/.exec(number)); // [ '1', index: 0, input: '12234567890', groups: undefined ]
console.log(/[1-5]/g.exec(number)); // [ '1', index: 0, input: '12234567890', groups: undefined ]

console.log(number.match(/[1-5]/)); // ['1']
console.log(number.match(/[1-5]/g)); // [ '1', '2', '3', '4', '5' ]

console.log(number.match(/4|5/g));
console.log(number.match(/4&5/g));
console.log(number2.match(/[^1]/g));

console.log(str.match(/[t.x]/g));

console.log(text.match(/\w/g));
console.log(text.match(/\W/g));
